var tendoTest = angular.module('tendoTest', []);

tendoTest.controller('AppController', function() {
  //placeholder
});

tendoTest.controller('StepController', function () {

    this.steps = [
        {'number': 1,
            'description': 'Step 1 Description'},
        {'number': 2,
            'description': 'Step 2 Description'},
        {'number': 3,
            'description': 'Step 3 Description'},
    ];

    this.stepTab = 1;

    this.setStep = function(newValue) {
        this.stepTab = newValue;
    };

    this.isSet = function(stepNumber) {
        return this.stepTab === stepNumber;
    };
});

